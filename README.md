# MePD neural circuitry comprised of kiss1, GABA and glutamate signalling


We extended a mathematical model of the ARC Kiss1 population (see Voliotis et al 2019: https://doi.org/10.1523/JNEUROSCI.0828-19.2019) to incorporate MePD regulation. The focus of this extended model is to test how NK3R activation in the MePD affects pulsatile LH secretion. The extended model incorporates: i) the disinhibitory GABA-GABA pathway from the MePD, ii) glutamatergic interneurons that in turn project to the GABA-GABA pathway, and iii) glutamatergic projections from the MePD onto the ARC iv) NK3R expression in the GABA population. This is a MATLAB implementation of the model.

