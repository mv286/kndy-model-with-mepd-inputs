% Implementation of the KNDy model with MePD inputs

function dx = extended_model_v2(t, x, params)

%% KNDy model parameterss 
% (see Voliotis et al 2019: https://doi.org/10.1523/JNEUROSCI.0828-19.2019)
d1 = params.d1;
d2 = params.d2;
d3 = params.d3;
p1 = params.p1;
p2 = params.p2;
p3 = params.p3;
ee = params.ee;
p3Basal = params.p3Basal;
p1_basal = params.p1_basal;
p2_basal = params.p2_basal;
maxRate = params.maxRate;
KD = params.KD;
KN = params.KN;
Kr1 =params.Kr1;
Kr2 =params.Kr2;
KI = params.KI;

%% MepD model parameters

d_E = params.d_E;
k_E = params.k_E;
r_E = params.r_E;
a_E = params.a_E;
theta_E = params.theta_E;

d_I = params.d_I;
k_I = params.k_I;
r_I = params.r_I;
a_I = params.a_I;
theta_I = params.theta_I;
theta_I2 = params.theta_I2;

c1 = params.c1;
c2 = params.c2;
c3 = params.c3;
c4 = params.c4;

c5 = params.c5;
c6 = params.c6;
c7 = params.c7;
c8 = params.c8;

kiss_input = params.kiss;
kiss = 1 + params.NKBa + 1/(1+ params.NKBant/1.2630 ) *(( params.stressor)/ (params.stressor + 1));


%% state variables
E    = x(1); % MePD Glut population activity 
I    = x(2); % MepD GABA population activity
I2   = x(3); % GABA interneurons activity
Dyn  = x(4); % Dyn levels
NKB  = x(5); % NKB levels
r    = x(6); % KNDy activity

%% model equations
dx = zeros(size(x));


%equation for MePD Glut neurons
E_input = c1*E - c2*I + kiss ;
dx(1)  =  d_E*(-E + (k_E-r_E*E)* (1./(1+exp(-a_E*(E_input-theta_E))) - 1./(1+exp(a_E*theta_E))));
%equation for MePD GABA neurons
I_input = c3*E - c4*I  + 1*kiss;
dx(2) =  d_I*(-I + (k_I-r_I*I)* (1./(1+exp(-a_I*(I_input-theta_I))) - 1./(1+exp(a_I*theta_I))));
%equation for GABA inter-neurons
I2_input = c5*E - c6*I  ;
dx(3) =  d_I*(-I2 + (k_I-r_I*I2)* (1./(1+exp(-a_I*(I2_input-theta_I2))) - 1./(1+exp(a_I*theta_I2))));
%equation for Dyn
dx(4) =  p1_basal + p1.*r.^2./(r.^2 + (Kr1).^2) - Dyn*d1;
%equation for NKB
dx(5) =  p2.*r.^2./(r.^2 + Kr2.^2)*KD.^2./(Dyn.^2 + KD.^2) + p2_basal - d2*NKB;
%equation for Kiss1 neurons
% I = basal + w * r ; synaptic weight w ranges in [0 1] and is a Function of NKB
p3_external =max(c7*E - c8*I2 + p3Basal , 0) ;
I = p3_external  + p3*( ee + (NKB.^2)./(NKB.^2 + KN.^2)  ).*r;
dx(6) =  maxRate * (KI./(exp(-I)+KI-1)-1) - d3*r;

end
