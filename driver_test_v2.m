close all;
clear all;


%% MODEL PARAMETERS
%% KNDy parameters
params.d1 = 0.2;
params.d2 = 1;
params.d3 = 10; 
params.p1 =4;
params.p2 = 40;
params.p3 = 2e-3;
params.maxRate = 30000;
params.p3Basal =  0.03;0.19; 
params.KD = 0.3; 
params.KN = 4;
params.Kr1 = 600;
params.Kr2 = 200;

params.ee = 0;
params.p1_basal = 0;
params.p2_basal = 0;
params.KI = 2;

%% MePD parameters

params.d_E = 1/(1/60/100);
params.k_E = 1;
params.r_E = 10;
params.a_E = 2.;
params.theta_E = 3.52;

params.d_I = 1/(1/60/100);
params.k_I = 1;
params.r_I = 10;
params.a_I = 2.17;
params.theta_I = 3.52;3.6;
params.theta_I2 = 3.52;

params.c1 = .5;
params.c2 = 0;
params.c3 = 0.;
params.c4 = 0.;
params.c5 = 50;
params.c6 = 10;
params.c7 = 0.0;
params.c8 = 2;
params.kiss = 4;
kiss_p = 2.8;

params.NKBa = 0.;
params.NKBant = 0.;
params.stressor = 0.;

%% simulation parameters 
params.Tmax =1000;
params.Dt =0.1;
params.Tspan = 0:params.Dt:params.Tmax;
params.dim = 6;
params.opt = [];
period_val = [0, 0];
params.opt = odeset('MaxStep',params.Dt);
paramstmp = params;

tmax2 =600;









%% plot surface

period_val = [];
val1 = logspace((1.1),(2),20);
val2 = logspace(-2,-0.5,20);



[X,Z] = meshgrid(val1,val2);


for i = 1:1:length(val1)
    for j = 1:1:length(val2)
        disp([i,j])
        params = paramstmp;
        params.stressor =  X(i,j);
        params.NKBant = Z(i,j);
        
        % run simulation
        X0 = zeros(1, params.dim); 
        [T, Y] = ode23s(@(t,y) extended_model_v2(t,y,params), params.Tspan , X0, params.opt);
        X0 = Y(end,:);
        tt = (params.Tmax-500);
        ft = T>=tt;
        timecourse_tmp = mean(Y(ft,6),2) ;
        ac = xcorr(timecourse_tmp,timecourse_tmp);  
        [~,loc] = findpeaks(ac./max(ac), 'MinPeakHeight',0.1);
        DT = params.Tspan(2)-params.Tspan(1);
        if isempty(max(diff(loc)*DT))        
            period_val(i,j) = 0 ;  
        else
            period_val(i,j) = max(diff(loc)*DT) ;    
        end

    end
end



surface(log10(X),log10(Z),period_val);
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);



%% PLOT the various activity profiles
%%
params.NKBa = 0;
params.NKBant = 0;
params.stressor = 0;

%% A) BASELINE
test_i = 101;

% simulate
X0 = zeros(1, params.dim); 
[T_baseline, Y_baseline] = ode23s(@(t,y) extended_model_v2(t,y,params), params.Tspan , X0, params.opt);

% plot results
figure(test_i);
tt = (params.Tspan(end)-60);
plot(T_baseline(T_baseline>=tt) - tt , Y_baseline(T_baseline>=tt ,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')



tt = (params.Tmax-60);
Y_baseline_60min = Y_baseline(T_baseline>=tt,:);
T_baseline_60min = T_baseline(T_baseline>=tt)-tt;


%% B)  + STRESSOR


test_i = 102;

params = paramstmp;
params.NKBa = 0;
params.NKBant = 0;
params.stressor = 10.^(1.43);

% run simulation using 
X0 = Y_baseline_60min(end,:);
params.Tspan = 0:params.Dt:90;
[T, Y] = ode23s(@(t,y) extended_model_v2(t,y,params), params.Tspan , X0, params.opt);

% plot results
figure(test_i);
plot(T_baseline_60min , Y_baseline_60min(:,6), 'k', 'linewidth', 1.5); hold on;
plot(T + T_baseline_60min(end) , Y(:,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')



% 
%% C)   STRESS + NKB receptor antagonist

test_i = 103;

params = paramstmp;
params.NKBa = 0;
params.NKBant = 10.^(1);
params.stressor = 10.^(1.43);





% run simulation
X0 = Y_baseline_60min(end,:); 
params.Tspan = 0:params.Dt:90;
[T, Y] = ode23s(@(t,y) extended_model_v2(t,y,params), params.Tspan , X0, params.opt);

% plot results
figure(test_i);
plot(T_baseline_60min , Y_baseline_60min(:,6), 'k', 'linewidth', 1.5); hold on;
plot(T + T_baseline_60min(end) , Y(:,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')



%% D)   NKB receptor agonist
  

test_i = 104;

params = paramstmp;
params.NKBa = 10.^(0);
params.NKBant = 0;
params.stressor = 0;

% run simulation
X0 = Y_baseline_60min(end,:); 
params.Tspan = 0:params.Dt:90;
[T, Y] = ode23s(@(t,y) extended_model_v2(t,y,params), params.Tspan , X0, params.opt);

% plot results
figure(test_i);

plot(T_baseline_60min , Y_baseline_60min(:,6), 'k', 'linewidth', 1.5); hold on;
plot(T + T_baseline_60min(end) , Y(:,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')

